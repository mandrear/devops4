from django.test import TestCase
from restaurant.models import Restaurant, Address

class AnimalTestCase(TestCase):
    def setUp(self):
        address = Address.objects.create(
            id=1,
            nb=32,
            street="Rue de la Gloire",
            zipcode=17000,
            city="La Rochelle"
        )
        Restaurant.objects.create(
            id=1,
            name="SeaPizz",
            address=address
        )

    def test_restaurant_exist(self):
        restaurant = Restaurant.objects.get(pk=1)
        print(restaurant)